Source: awscli
Section: admin
Priority: optional
Maintainer: Debian Cloud Team <debian-cloud@lists.debian.org>
Uploaders:
    Ross Vandegrift <rvandegrift@debian.org>,
    Noah Meyerhans <noahm@debian.org>,
    Thomas Bechtold <thomasbechtold@jpberlin.de>,
Rules-Requires-Root: no
Build-Depends:
	debhelper-compat (= 12),
	cmake,
	dh-python,
	python3-dev:any,
	python3-setuptools,
	python3-awscrt (>= 0.23.4),
Build-Depends-Indep:
	tox,
	python3-build,
	python3-colorama,
	python3-cryptography,
	python3-dateutil,
	python3-prompt-toolkit,
	python3-ruamel.yaml,
	python3-wheel,
	flit,
	pybuild-plugin-pyproject,
	python3-distro,
	python3-jmespath,
Standards-Version: 4.6.1
Homepage: https://github.com/aws/aws-cli
Vcs-Git: https://salsa.debian.org/cloud-team/awscli.git
Vcs-Browser: https://salsa.debian.org/cloud-team/awscli

Package: awscli
Architecture: all
Depends:
	groff-base,
	python3,
	python3-pyasn1,
	${python3:Depends},
	${misc:Depends}
Description: Unified command line interface to Amazon Web Services
 The AWS CLI provides direct access to the public APIs of AWS
 services. You can explore a service's capabilities with the AWS CLI,
 and develop shell scripts to manage your resources. In addition to
 the low-level, API-equivalent commands, several AWS services provide
 customizations for the AWS CLI. Customizations can include
 higher-level commands that simplify using a service with a complex
 API.
