[build-system]
requires = [
"flit_core>=3.7.1,<3.9.1",
]
build-backend = "pep517"
backend-path = ["backends"]

[project]
name = "awscli"
description = "Universal Command Line Environment for AWS."
authors = [
  {name = "Amazon Web Services"},
]
license = { file = "LICENSE.txt" }
readme = "README.rst"
requires-python = ">=3.8"
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Intended Audience :: Developers",
    "Intended Audience :: System Administrators",
    "Natural Language :: English",
    "License :: OSI Approved :: Apache Software License",
    "Programming Language :: Python",
    'Programming Language :: Python :: 3 :: Only',
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    'Programming Language :: Python :: 3.12',
]
dependencies = [
    "colorama>=0.2.5,<0.4.7",
    "docutils>=0.10,<0.20",
    "cryptography>=40.0.0,<43.0.2",
    "ruamel.yaml>=0.15.0,<=0.17.21",
    # ruamel.yaml only requires ruamel.yaml.clib for Python versions
    # less than or equal to Python 3.10. In order to ensure we have
    # a consistent dependency closure across all Python versions,
    # we explicitly include ruamel.yaml.clib as a dependency.
    "ruamel.yaml.clib>=0.2.0,<=0.2.8",
    "prompt-toolkit>=3.0.24,<3.0.39",
    "distro>=1.5.0,<1.9.0",
    "awscrt==0.23.4",
    "python-dateutil>=2.1,<=2.9.0",
    "jmespath>=0.7.1,<1.1.0",
    "urllib3>=1.25.4,<1.27",
    # zipp>=3.21.0 dropped support for Python 3.8. In order to ensure
    # we have a consistent dependency closure across all Python
    # versions, we explicitly include zipp as a dependency.
    "zipp<3.21.0",
]
dynamic = ["version"]

[project.urls]
homepage = "http://aws.amazon.com/cli/"

[tool.awscli.sdist]
include = [
    "backends/**/*.py",
    "bin/*",
    "CHANGELOG.rst",
    "tests/**/*",
    "requirements/**/*.txt",
    "configure",
    "Makefile.in",
    "exe/**/*",
]
# end of cli sdist tool section

[tool.pytest.ini_options]
# We set error warning filters in order to enforce that particular
# undesired warnings are not emitted as part of the codebase. If you
# want to override these filters with pytest's default warning filters
# (i.e. print out deprecation warnings instead of erroring out on them),
# you can run pytest with the -Wd flag.
filterwarnings = [
    # Prevents declaring tests that cannot be collected by pytest
    "error::pytest.PytestCollectionWarning",

    # Prevents use of functionality that is deprecated or pending deprecation
    "error::DeprecationWarning",
    "error::PendingDeprecationWarning",

    # Python 3.12 introduced a new DeprecationWarning in python-dateutil's
    # datetime.now() functions and they're scheduled for removal in a future
    # version. When we fully switch over to timezone-aware calls instead,
    # we should be able to remove these filters.
    'default:datetime\.datetime\.utcnow\(\) is deprecated:DeprecationWarning',

    # Python 3.12 introduced a new DeprecationWarning in python-dateutil's
    # datetime.utcfromtimestamp() functions and they're scheduled for removal in
    # a future version. When we fully switch over to timezone-aware calls instead,
    # we should be able to remove these filters.
    'default:datetime\.datetime\.utcfromtimestamp\(\) is deprecated:DeprecationWarning',

    # Python 3.12 introduced a new DeprecationWarning in calls to ast.Str, which
    # flit-core uses. When they release the removal of such calls, we should
    # be able to remove these filters.
    'default:ast\.Str is deprecated and will be removed:DeprecationWarning',

    # Python 3.12 introduced a new DeprecationWarning in flit-core. When they release the
    # removal of reference to 'attribute s', we should be able to remove these filters.
    'default:Attribute s is deprecated and will be removed:DeprecationWarning',

    # Python 3.12 introduced a new DeprecationWarning which gets raised in our call to build_sdist of
    # pep517 backend. When we explicitly address this change, we should be able to remove these filters.
    'default:Python 3\.14 will, by default, filter extracted tar archives and reject files or modify their metadata:DeprecationWarning',

    # Do not error out on deprecation warnings stemming from our current
    # version of setuptools (57.5.0) in a Python 3.10 environment. When we add
    # support for the latest version of setuptools, we should be able to remove
    # these filters.
    'default:The distutils\.sysconfig module is deprecated:DeprecationWarning',

    # Python 3.10.9 introduced a new DeprecationWarning for the implicit
    # creation of event loops. Python plans to remove this warning in the
    # next patch. This filter exception should also be removed once it's
    # confirmed that tests are passing again in the latest Python 3.10.
    # https://github.com/python/cpython/issues/100160
    'default:There is no current event loop:DeprecationWarning',

    # pytest-cov is incompatible with pytest-xdist 3.x because pytest-cov
    # uses the config variable for rsyncdir. This filter exception should
    # be able to be removed once both of the following issues are resolved.
    # https://github.com/pytest-dev/pytest-cov/issues/557
    # https://github.com/pytest-dev/pytest-xdist/issues/825
    'default:The --rsyncdir command line argument and rsyncdirs config variable are deprecated.:DeprecationWarning'
]
markers = [
    "slow: marks tests as slow",
    "validates_models: marks tests as one which validates service models",
]

[tool.black]
line-length = 80

[tool.ruff]
exclude = [
    ".bzr",
    ".direnv",
    ".eggs",
    ".git",
    ".git-rewrite",
    ".hg",
    ".ipynb_checkpoints",
    ".mypy_cache",
    ".nox",
    ".pants.d",
    ".pyenv",
    ".pytest_cache",
    ".pytype",
    ".ruff_cache",
    ".svn",
    ".tox",
    ".venv",
    ".vscode",
    "__pypackages__",
    "_build",
    "buck-out",
    "build",
    "dist",
    "node_modules",
    "site-packages",
    "venv",
]

force-exclude = true

# Format same as Black.
line-length = 79
indent-width = 4

[tool.ruff.lint]
# Enable Pyflakes (`F`) and a subset of the pycodestyle (`E`)  codes by default.
# Unlike Flake8, Ruff doesn't enable pycodestyle warnings (`W`) or
# McCabe complexity (`C901`) by default.
select = ["E4", "E7", "E9", "F", "I", "UP"]
ignore = ["F401"]

# Allow fix for all enabled rules (when `--fix`) is provided.
fixable = ["ALL"]
unfixable = []

# Allow unused variables when underscore-prefixed.
dummy-variable-rgx = "^(_+|(_+[a-zA-Z0-9_]*[a-zA-Z0-9]+?))$"

[tool.ruff.format]
# Like Black, use double quotes for strings, spaces for indents
# and trailing commas.
quote-style = "preserve"
indent-style = "space"
skip-magic-trailing-comma = false
line-ending = "auto"

docstring-code-format = false
docstring-code-line-length = "dynamic"
